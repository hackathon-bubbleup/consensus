#!/bin/bash -ex

java -version  2>&1 >/dev/null
mvn --version 2>&1 >/dev/null

cur="$PWD"
mvn clean install -DskipTests


rm -rf build
mkdir -p build
cd build/
git clone https://gitlab.com/hackathon-bubbleup/consensus-fe
cd consensus-fe
npm install
npm run build
cd $cur

docker-compose up -d --build
