FROM java:8-jre
COPY target/consensus-backend.jar /consensus-backend.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/consensus-backend.jar"]