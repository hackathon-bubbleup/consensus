package com.cs.hackaton.controller;

import com.cs.hackaton.BackEndApplication;
import com.cs.hackaton.conf.TestConfiguration;
import com.cs.hackaton.entities.Participant;
import com.cs.hackaton.entities.Round;
import com.cs.hackaton.entities.Session;
import com.cs.hackaton.entities.Trivia;
import com.cs.hackaton.repositories.RoundRepository;
import com.cs.hackaton.repositories.SessionRepository;
import com.cs.hackaton.service.SessionService;
import com.cs.hackaton.util.TestUtilProvider;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        classes = {TestConfiguration.class, BackEndApplication.class})
public class SessionTestIT {
    private static final Participant PRESENTER = new Participant("presenter", "token");
    private static final String TEST_SESSION = "test";
    private static final String TEST_QUESTION = "What's life meaning?";

    @Autowired
    private WebTestClient webClient;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private RoundRepository roundRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private FirebaseMessaging firebaseMessaging;
    @Autowired
    private TestUtilProvider testUtilProvider;

    @Before
    public void setUp() {
        sessionRepository.deleteAll().block();
        roundRepository.deleteAll().block();
    }

    @Test
    public void startSession_EmptyBody_WhenSessionNotFound() {
        Trivia trivia = Trivia.builder()
                .question("What's life meaning?")
                .build();

        webClient.post().uri("/api/sessions/{sessionName}/start", TEST_SESSION)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(trivia), Trivia.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .isEmpty();
    }

    @Test
    public void startSession_CreateTriviaAndUpdateSession() {
        Trivia trivia = Trivia.builder()
                .question(TEST_QUESTION)
                .build();

        testUtilProvider.createOrUpdateSession(TEST_SESSION, PRESENTER);

        Session responseBody = callStartSession(trivia);

        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getStatus()).isEqualTo(Session.SessionStatus.LOCKED);

        Round savedRound = roundRepository.findOneBySessionId(responseBody.getId()).block();

        assertThat(savedRound).isNotNull();
        assertThat(savedRound.getTrivia()).isNotNull();
        Assert.assertEquals(trivia.getQuestion(), savedRound.getTrivia().getQuestion());
    }

    @Test
    public void joinSession_AddParticipantAndNotifyOthers() throws FirebaseMessagingException {
        Participant newParticipant = new Participant("Morty", "123");

        testUtilProvider.createOrUpdateSession(TEST_SESSION, PRESENTER, new Participant("Rick", "123"));

        sessionService.joinSession(TEST_SESSION, newParticipant).block();

        verify(firebaseMessaging, times(3)).send(any());

        Session updatedSession = sessionService.findByName(TEST_SESSION).block();

        assertThat(updatedSession).isNotNull();
        assertThat(updatedSession.getParticipants().size()).isEqualTo(2);
        assertThat(updatedSession.getParticipants()).contains(newParticipant);
    }

    private Session callStartSession(Trivia trivia) {
        return webClient.post().uri("/api/sessions/{sessionName}/start", TEST_SESSION)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(trivia), Trivia.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Session.class)
                .returnResult()
                .getResponseBody();
    }
}