package com.cs.hackaton.service;

import com.cs.hackaton.BackEndApplication;
import com.cs.hackaton.conf.TestConfiguration;
import com.cs.hackaton.entities.*;
import com.cs.hackaton.util.TestUtilProvider;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static com.cs.hackaton.entities.Answer.Status.KEEP;
import static com.cs.hackaton.entities.Round.Status.START;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        classes = {TestConfiguration.class, BackEndApplication.class})
public class UserAnswerServiceIT {
    public static final Participant TEST_PRESENTER = new Participant("Joe", "1");
    public static final Participant MIKE = new Participant("Mike", "2");
    public static final Participant RAY = new Participant("Ray", "3");
    public static final String TEST_SESSION = "test";

    @Autowired
    private UserAnswerService service;
    @Autowired
    private TestUtilProvider testUtilProvider;
    @Autowired
    private RoundService roundService;
    @Autowired
    private FirebaseMessaging firebaseMessaging;

    @Test
    public void updateAnswer_CreateAggregationResultAndNotifyEveryOne() throws FirebaseMessagingException {
        Session session = testUtilProvider.createOrUpdateSession(TEST_SESSION, TEST_PRESENTER, MIKE, RAY);

        Trivia trivia = Trivia.builder()
                .question("WTF man?")
                .answers(answers(answer(0, "IDK", KEEP),
                        answer(1, "OMG", KEEP),
                        answer(2, "0_o", KEEP)))
                .build();

        Round round1 = roundService.save(Round.builder()
                .sessionId(session.getId())
                .number(0)
                .trivia(trivia)
                .status(START)
                .build())
                .block();

        UserAnswerEntity answer1 = answerE("IDK", round1.getId());
        UserAnswerEntity answer2 = answerE("IDK", round1.getId());

        UserAnswerEntity answer3 = answerE("OMG", round1.getId());
        UserAnswerEntity answer4 = answerE("OMG", round1.getId());

        service.updateAndNotifyAll(answer1).block();
        service.updateAndNotifyAll(answer2).block();
        service.updateAndNotifyAll(answer3).block();
        List<AggregateAnswer> aggregateAnswerList1 = service.updateAndNotifyAll(answer4).block();

        assertThat(aggregateAnswerList1).isNotNull();
        assertThat(aggregateAnswerList1.size()).isEqualTo(3);
        assertThat(aggregateAnswerList1.get(0).getVoteCount()).isEqualTo(2);
        assertThat(aggregateAnswerList1.get(1).getVoteCount()).isEqualTo(2);
        assertThat(aggregateAnswerList1.get(2).getVoteCount()).isEqualTo(0);

        verify(firebaseMessaging, times(12)).send(any()); // (2 participants + presenter) * 4 times
    }

    private List<Answer> answers(Answer... answers) {
        return Arrays.asList(answers);
    }

    private Answer answer(int index, String answer, Answer.Status status) {
        return new Answer(index, answer, status);
    }

    private UserAnswerEntity answerE(String text, String roundId) {
        return UserAnswerEntity.builder()
                .answerText(text)
                .roundId(roundId)
                .sessionName(TEST_SESSION)
                .build();
    }
}