package com.cs.hackaton.conf;

import com.cs.hackaton.util.TestUtilProvider;
import com.google.firebase.messaging.FirebaseMessaging;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

@org.springframework.boot.test.context.TestConfiguration
public class TestConfiguration {
    @MockBean
    private FirebaseMessaging firebaseMessaging;

    @Bean
    public TestUtilProvider testUtilProvider() {
        return new TestUtilProvider();
    }
}
