package com.cs.hackaton.repositories;

import com.cs.hackaton.BackEndApplication;
import com.cs.hackaton.conf.TestConfiguration;
import com.cs.hackaton.entities.Participant;
import com.cs.hackaton.entities.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        classes = {TestConfiguration.class, BackEndApplication.class})
public class SessionRepositoryTest {
    @Autowired
    private SessionRepository sessionRepository;

    @Before
    public void setUp() {
        sessionRepository.deleteAll().block();
    }

    @Test
    public void save_and_find_sameSession() {
        Session session = Session.builder()
                .id("1")
                .topicName("Test")
                .presenter(new Participant("abc", "cbe"))
                .build();

        sessionRepository.save(session).block();

        Mono<Session> foundSession = sessionRepository.findById("1");

        assertThat(foundSession.block()).isEqualTo(session);
    }
}
