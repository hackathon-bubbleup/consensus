package com.cs.hackaton.util;

import com.cs.hackaton.entities.Participant;
import com.cs.hackaton.entities.Session;
import com.cs.hackaton.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

public class TestUtilProvider {
    @Autowired
    private SessionService sessionService;

    public Session createOrUpdateSession(String sessionName, Participant presenter, Participant... participants) {
        Session session = Session.builder()
                .presenter(presenter)
                .topicName(sessionName)
                .participants(Arrays.asList(participants))
                .build();

        return sessionService.saveOrUpdateSession(session).block();
    }
}
