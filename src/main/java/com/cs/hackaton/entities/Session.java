package com.cs.hackaton.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Document(collection = "sessions")
public class Session {
    @Id
    String id;
    @NotNull(message = "Topic Name can't be null")
    String topicName;
    @NotNull(message = "Presenter Name can't be null")
    Participant presenter;
    @Builder.Default
    List<Participant> participants = new ArrayList<>();
    SessionStatus status;

    public enum SessionStatus {
        OPEN,
        LOCKED,
        ENDED
    }
}
