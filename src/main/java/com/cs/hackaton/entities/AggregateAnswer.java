package com.cs.hackaton.entities;

import lombok.Data;

@Data
public class AggregateAnswer extends Answer {
    int voteCount;

    public static AggregateAnswer from(Answer answer) {
        AggregateAnswer aggregateAnswer = new AggregateAnswer();
        aggregateAnswer.setIndex(answer.getIndex());
        aggregateAnswer.setStatus(answer.getStatus());
        aggregateAnswer.setText(answer.getText());
        return aggregateAnswer;
    }
}
