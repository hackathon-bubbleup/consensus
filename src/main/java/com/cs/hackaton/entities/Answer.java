package com.cs.hackaton.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Answer {
    int index;
    String text;
    Status status = Status.KEEP;

    public enum Status {
        KEEP, REMOVED
    }
}
