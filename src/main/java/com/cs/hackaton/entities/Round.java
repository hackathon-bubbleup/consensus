package com.cs.hackaton.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document("rounds")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Round {

    @Id
    String id;
    String sessionId;
    Trivia trivia;
    int number;
    Status status = Status.START;
    List<AggregateAnswer> aggregateAnswers;


    public static enum Status {
        START,
        END
    }
}
