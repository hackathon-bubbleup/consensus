package com.cs.hackaton.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "userAnswers")
public class UserAnswerEntity {
    @Id
    private String id;
    @NotNull
    private String sessionName;
    private String roundId;
    @NotNull
    private String answerText;
    @NotNull
    private Participant participant;
}
