package com.cs.hackaton.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Participant {
    @NotNull
    String nickname;
    @NotNull
    String fcmToken;
}
