package com.cs.hackaton.controller;

import com.cs.hackaton.entities.Participant;
import com.cs.hackaton.entities.Round;
import com.cs.hackaton.entities.Session;
import com.cs.hackaton.entities.Trivia;
import com.cs.hackaton.service.RoundService;
import com.cs.hackaton.service.SessionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@Slf4j
public class SessionController {
    private SessionService sessionService;
    private RoundService roundService;

    @PostMapping(value = "/api/sessions")
    @ResponseBody
    public Mono<Session> createSession(@RequestBody @Valid Session session) {
        session.setStatus(Session.SessionStatus.OPEN);
        return sessionService.createSession(session);
    }

    @PostMapping(value = "/api/sessions/{sessionName}/join")
    @ResponseBody
    public Mono<Session> joinSession(@PathVariable String sessionName, @RequestBody Participant participant) {
        return sessionService.joinSession(sessionName, participant);
    }

    @PutMapping(value = "/api/sessions/{sessionName}/status")
    @ResponseBody
    public ResponseEntity<?> setStatus(@PathVariable String sessionName, @RequestParam Session.SessionStatus status) {
        return ResponseEntity.ok(sessionService.setStatus(sessionName, status));
    }

    @PostMapping(value = "/api/sessions/{sessionName}/start")
    @ResponseBody
    public ResponseEntity<?> startSession(@PathVariable String sessionName, @RequestBody Trivia trivia) {
        return ResponseEntity.ok(sessionService.startSession(sessionName, trivia));
    }

    @PostMapping(value = "/api/rounds/{sessionName}/finish")
    @ResponseBody
    public Mono<Round> finishRound(@PathVariable("sessionName") String sessionName, @RequestBody Participant presenter) {
        return sessionService.endRound(sessionName, presenter);

    }
}
