package com.cs.hackaton.controller;

import com.cs.hackaton.entities.Round;
import com.cs.hackaton.service.RoundService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@RestController
public class RoundController {
    private RoundService roundService;

    @PostMapping("/api/rounds")
    @ResponseBody
    public Mono<Round> createRound(@RequestBody Round newRound) {
        return roundService.save(newRound);
    }
}
