package com.cs.hackaton.controller;

import com.cs.hackaton.entities.UserAnswerEntity;
import com.cs.hackaton.service.UserAnswerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController("/api/answers")
@AllArgsConstructor
public class AnswerController {

    private UserAnswerService userAnswerService;

    @PutMapping
    @ResponseBody
    public Mono<?> putAnswer(@RequestBody UserAnswerEntity userAnswerEntity){
        return userAnswerService.updateAndNotifyAll(userAnswerEntity);
    }
}
