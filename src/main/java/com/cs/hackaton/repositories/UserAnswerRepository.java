package com.cs.hackaton.repositories;

import com.cs.hackaton.entities.UserAnswerEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface UserAnswerRepository extends ReactiveMongoRepository<UserAnswerEntity, String> {
    Flux<UserAnswerEntity> findAllByRoundIdAndSessionName(String roundId, String sessionName);
}
