package com.cs.hackaton.repositories;

import com.cs.hackaton.entities.Round;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface RoundRepository extends ReactiveMongoRepository<Round, String> {
    Mono<Round> findOneBySessionId(String sessionId);

    Flux<Round> findAllBySessionId(String sessionId);
}
