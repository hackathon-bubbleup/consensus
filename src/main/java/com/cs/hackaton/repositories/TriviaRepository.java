package com.cs.hackaton.repositories;

import com.cs.hackaton.entities.Trivia;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface TriviaRepository extends ReactiveMongoRepository<Trivia, String> {
    Mono<Trivia> findOneBySessionId(String sessionId);
}
