package com.cs.hackaton.repositories;

import com.cs.hackaton.entities.Session;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface SessionRepository extends ReactiveMongoRepository<Session, String> {
    Mono<Session> findOneByTopicName(String name);
    Mono<Boolean> existsByTopicName(String topicName);
}