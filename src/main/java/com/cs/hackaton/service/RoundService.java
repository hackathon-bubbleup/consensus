package com.cs.hackaton.service;

import com.cs.hackaton.entities.Round;
import com.cs.hackaton.repositories.RoundRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class RoundService {
    private RoundRepository roundRepository;

    public Mono<Round> save(Round newRound) {
        return roundRepository.save(newRound);
    }

    public Mono<Round> updateStatus(Round round, Round.Status status) {
        round.setStatus(status);
        return this.save(round);
    }

    private Flux<Round> getBySessionId(String sessionId) {
        return roundRepository.findAllBySessionId(sessionId);
    }

    public Mono<Round> getCurrentRound(String sessionId) {
        return getBySessionId(sessionId)
                .reduce((a, b) -> (a.getNumber() > b.getNumber()) ? a : b);
    }
}
