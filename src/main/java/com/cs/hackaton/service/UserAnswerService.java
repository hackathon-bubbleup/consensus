package com.cs.hackaton.service;

import com.cs.hackaton.entities.AggregateAnswer;
import com.cs.hackaton.entities.Session;
import com.cs.hackaton.entities.UserAnswerEntity;
import com.cs.hackaton.repositories.SessionRepository;
import com.cs.hackaton.repositories.UserAnswerRepository;
import com.cs.hackaton.service.firebase.FireBaseService;
import com.google.cloud.Tuple;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;

import java.util.List;

@Service
@AllArgsConstructor
public class UserAnswerService {
    private UserAnswerRepository userAnswerRepository;
    private SessionRepository sessionRepository;
    private RoundService roundService;
    private AnswerAggregationService answerAggregationService;
    private FireBaseService fireBaseService;

    public Mono<List<AggregateAnswer>> updateAndNotifyAll(UserAnswerEntity userAnswerEntity) {
        return findSessionByName(userAnswerEntity)
                .flatMap(session -> roundService.getCurrentRound(session.getId())
                        .map(round -> Tuples.of(session, round))
                )
                .flatMap(sessionRound -> {
                    userAnswerEntity.setRoundId(sessionRound.getT2().getId());
                    return userAnswerRepository.save(userAnswerEntity).map(uae -> Tuples.of(sessionRound.getT1(), sessionRound.getT2(), uae));
                })
                .log()
                .flatMap(sessionRoundUser -> answerAggregationService.getAggregatedAnswers(sessionRoundUser.getT1().getTopicName(), sessionRoundUser.getT2())
                        .collectList()
                        .map(aggregateAnswers -> Tuples.of(sessionRoundUser.getT1(), sessionRoundUser.getT2(), sessionRoundUser.getT3(), aggregateAnswers))
                )
                .log()
                .flatMap(sessionRoundUserAnswerList -> fireBaseService.notifyQuestionAnswered(sessionRoundUserAnswerList.getT1(), sessionRoundUserAnswerList.getT4())
                        .collectList()
                        .map(participants -> sessionRoundUserAnswerList.getT4())
                );
    }

    private Mono<Session> findSessionByName(UserAnswerEntity userAnswerEntity) {
        return sessionRepository.findOneByTopicName(userAnswerEntity.getSessionName());
    }
}
