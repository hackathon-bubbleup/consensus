package com.cs.hackaton.service;

import com.cs.hackaton.entities.*;
import com.cs.hackaton.repositories.SessionRepository;
import com.cs.hackaton.service.firebase.FireBaseService;
import lombok.AllArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class SessionService {

    private SessionRepository sessionRepository;
    private FireBaseService fireBaseService;
    private RoundService roundService;
    private AnswerAggregationService answerAggregationService;

    public Mono<Session> findByName(String sessionName) {
        return sessionRepository.findOneByTopicName(sessionName);
    }

    public Mono<Session> createSession(Session sessionEntity) {
        return Mono.just(sessionEntity)
                .flatMap(object -> Mono.just(object)
                        .filterWhen(s -> sessionRepository.existsByTopicName(s.getTopicName()).map(aBoolean -> !aBoolean))
                        .switchIfEmpty(Mono.error(new RuntimeException("Session Already exists")))
                )
                .flatMap(p -> sessionRepository.insert(p));
    }

    public Mono<Session> saveOrUpdateSession(Session sessionEntity) {
        return sessionRepository.save(sessionEntity);
    }

    public Mono<Session> joinSession(String sessionName, Participant participant) {
        return sessionRepository.findOneByTopicName(sessionName)
                .flatMap(session -> Mono.just(session).switchIfEmpty(Mono.error(new RuntimeException("Session does not Exists"))))
                .map(session -> {
                    if (CollectionUtils.isEmpty(session.getParticipants())) {
                        session.setParticipants(new ArrayList<>());
                    } else {
                        boolean b = session.getParticipants().stream()
                                .anyMatch(participant1 -> participant1.getNickname().equalsIgnoreCase(participant.getNickname()));
                        if (b) {
                            throw new RuntimeException("Participant exists");
                        }
                    }
                    session.getParticipants().add(participant);
                    return session;
                })
                .flatMap(this::saveOrUpdateSession)
                .flatMap(session -> fireBaseService.notifyParticipantJoined(session).collectList()
                        .map(participants -> Tuples.of(session, participants)))
                .map(Tuple2::getT1);
    }

    public Mono<Session> setStatus(String name, Session.SessionStatus sessionStatus) {
        return sessionRepository.findOneByTopicName(name)
                .flatMap(session -> Mono.just(session).switchIfEmpty(Mono.error(new RuntimeException("Session does not Exists"))))
                .map(session -> {
                    session.setStatus(sessionStatus);
                    return session;
                })
                .flatMap(this::saveOrUpdateSession);
    }

    public Mono<Session> startSession(String sessionName, Trivia newTrivia) {
        Mono<Session> sessionMono = sessionRepository.findOneByTopicName(sessionName)
                .map(session -> {
                    Trivia trivia = createTrivia(session.getId(), newTrivia);
                    Round round = Round.builder()
                            .number(0)
                            .sessionId(session.getId())
                            .trivia(trivia)
                            .aggregateAnswers(new ArrayList<>())
                            .build();
                    return createPair(session, round);
                })
                .flatMap(pair ->
                        Mono.just(pair.getFirst())
                                .zipWith(roundService.save(pair.getSecond()))
                )
                .flatMap(pair ->
                        fireBaseService.notifySessionStarted(pair.getT1(), pair.getT2().getTrivia()).collect(Collectors.toList())
                                .map(participants -> Tuples.of(pair.getT1(), pair.getT2(), participants))
                )
                .flatMap(tuple3 -> {
                    Session s = tuple3.getT1().toBuilder()
                            .status(Session.SessionStatus.LOCKED)
                            .build();
                    return sessionRepository.save(s);
                });

        return sessionMono;
    }

    private Trivia createTrivia(String sessionId, Trivia trivia) {
        return trivia.toBuilder()
                .sessionId(sessionId)
                .build();
    }

    private Pair<Session, Round> createPair(Session session, Round round) {
        return Pair.of(session, round);
    }

    public Mono<Round> endRound(String sessionName, Participant presenter) {
        return findByName(sessionName)
                .flatMap(session -> roundService.getCurrentRound(session.getId()).map(round -> Tuples.of(session, round)))
                .log()
                .flatMap(sessionRound -> answerAggregationService.getAggregatedAnswers(sessionName, sessionRound.getT2()).collectList().map(ans -> {
                    Round round = sessionRound.getT2();
                    List<AggregateAnswer> keptQuestions = ans.stream()
                            .filter(a -> a.getStatus().equals(Answer.Status.KEEP))
                            .collect(Collectors.toList());
                    if (keptQuestions.size() > 1) {
                        AggregateAnswer removedAnswer = keptQuestions.stream()
                                .sorted(Comparator.comparingInt(AggregateAnswer::getVoteCount))
                                .collect(Collectors.toList())
                                .get(0);

                        removedAnswer.setStatus(Answer.Status.REMOVED);
                    }
                    round.setAggregateAnswers(ans);
                    return Tuples.of(sessionRound.getT1(), round);
                }))
                .log()
                .flatMap(sessionRound -> roundService.updateStatus(sessionRound.getT2(), Round.Status.END)
                        .map(round -> Tuples.of(sessionRound.getT1(), round))
                        .flatMap(tuple2 -> fireBaseService.notifyRoundEnded(tuple2.getT1(), tuple2.getT2())
                                .collectList()
                                .map(x -> tuple2.getT2())
                        )
                );
    }
}
