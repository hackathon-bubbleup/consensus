package com.cs.hackaton.service.firebase;

import com.cs.hackaton.entities.*;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.WebpushConfig;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static com.cs.hackaton.service.firebase.FireBaseMessageType.*;
import static com.cs.hackaton.utility.JsonUtil.generateJson;

@Slf4j
public class FireBaseService {
    private final FirebaseMessaging firebaseMessaging;

    public FireBaseService(FirebaseMessaging firebaseMessaging) {
        this.firebaseMessaging = firebaseMessaging;
    }

    public String sendMessageForToken(String type, String payload, String clientToken) {
        return sendMessage(message(type, payload, clientToken));
    }

    public String sendMessage(Message message) {
        try {
            return firebaseMessaging.send(message);
        } catch (FirebaseMessagingException e) {

            throw new RuntimeException("Was unable to send firebase message.", e);
        }
    }

    public Flux<Participant> notifyQuestionAnswered(Session session, List<AggregateAnswer> aggregateAnswers) {
        String payload = generateJson(new QuestionAnsweredNotification(aggregateAnswers));
        return Flux.fromIterable(session.getParticipants())
                .mergeWith(Mono.just(session.getPresenter()))
                .doOnNext(participant -> {
                    Message message = message(USER_ANSWER.name(), payload, participant.getFcmToken());
                    sendMessage(message);
                });
    }

    public Flux<Participant> notifyParticipantJoined(Session session) {
        String payload = generateJson(session.getParticipants().get(session.getParticipants().size() - 1));
        return Flux.fromIterable(session.getParticipants())
                .mergeWith(Mono.just(session.getPresenter()))
                .doOnNext(participant -> {
                    Message message = message(PARTICIPANT_JOINED.name(), payload, participant.getFcmToken());
                    sendMessage(message);
                });
    }

    public Flux<Participant> notifyRoundEnded(Session session, Round round) {
        String payload = generateJson(round);
        return Flux.fromIterable(session.getParticipants())
                .mergeWith(Mono.just(session.getPresenter()))
                .doOnNext(participant -> {
                    Message message = message(END_ROUND.name(), payload, participant.getFcmToken());
                    sendMessage(message);
                });
    }

    public Flux<Participant> notifySessionStarted(Session session, Trivia trivia) {
        String payload = generateJson(trivia);
        return Flux.fromIterable(session.getParticipants())
                .mergeWith(Mono.just(session.getPresenter()))
                .doOnNext(participant -> {
                    Message message = message(SESSION_STARTED.name(), payload, participant.getFcmToken());
                    sendMessage(message);
                });
    }

    private Message message(String type, String payload, String clientToken) {
        return Message.builder()
                .putData("TYPE", type)
                .putData("message", payload)
                .setToken(clientToken)
                .setWebpushConfig(WebpushConfig.builder().putHeader("urgency", "high").build())
                .build();
    }
}
