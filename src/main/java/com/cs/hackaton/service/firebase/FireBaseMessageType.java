package com.cs.hackaton.service.firebase;

public enum FireBaseMessageType {
    SESSION_STARTED,
    PARTICIPANT_JOINED,
    USER_ANSWER,
    END_ROUND;
}
