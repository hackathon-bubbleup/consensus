package com.cs.hackaton.service.firebase;

import com.cs.hackaton.entities.AggregateAnswer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionAnsweredNotification {
    List<AggregateAnswer> triviaState;
}
