package com.cs.hackaton.service;

import com.cs.hackaton.entities.AggregateAnswer;
import com.cs.hackaton.entities.Answer;
import com.cs.hackaton.entities.Round;
import com.cs.hackaton.entities.UserAnswerEntity;
import com.cs.hackaton.repositories.SessionRepository;
import com.cs.hackaton.repositories.UserAnswerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class AnswerAggregationService {
    private UserAnswerRepository userAnswerRepository;
    private SessionRepository sessionRepository;
    private RoundService roundService;

    public Flux<AggregateAnswer> getAggregatedAnswers(String sessionName, Round round) {
        return sessionRepository.findOneByTopicName(sessionName)
                .flatMap(session -> roundService.getCurrentRound(session.getId()))
                .flatMap(r -> internalGetAggregateAnswers(sessionName, round).map(aggregateAnswers -> Tuples.of(r.getTrivia().getAnswers(), aggregateAnswers)))
                .map(answerAggregateAnswer -> {
                    List<Answer> answers = answerAggregateAnswer.getT1();
                    List<AggregateAnswer> aggregateAnswers = answerAggregateAnswer.getT2();
                    return answers.stream().map(AggregateAnswer::from)
                            .map(a -> {
                                int voteCount = aggregateAnswers.stream()
                                        .filter(aa -> aa.getText().equalsIgnoreCase(a.getText()))
                                        .findFirst()
                                        .map(AggregateAnswer::getVoteCount)
                                        .orElse(0);
                                a.setVoteCount(voteCount);
                                return a;
                            })
                            .collect(Collectors.toList());
                })
                .flatMapMany(Flux::fromIterable);
    }

    private Mono<List<AggregateAnswer>> internalGetAggregateAnswers(String sessionName, Round round) {
        return userAnswerRepository.findAllByRoundIdAndSessionName(round.getId(), sessionName)
                .groupBy(UserAnswerEntity::getAnswerText, answer -> answer)
                .flatMap(it -> {
                    AggregateAnswer answer = new AggregateAnswer();
                    answer.setText(it.key());
                    return it.reduce(answer, (a, b) -> {
                        a.setVoteCount(a.getVoteCount() + 1);
                        return a;
                    });
                })
                .collectList()
                ;
    }
}
