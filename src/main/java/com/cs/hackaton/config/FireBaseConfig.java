package com.cs.hackaton.config;

import com.cs.hackaton.service.firebase.FireBaseService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;

@Configuration
public class FireBaseConfig {
    @ConditionalOnProperty(prefix = "firebase", name = "enabled", havingValue = "true")
    @Bean
    public FirebaseApp firebaseApp() throws IOException {
        InputStream key = FireBaseConfig.class.getResourceAsStream("/key.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(key))
                .setDatabaseUrl("https://consensus-hack2018.firebaseio.com")
                .build();

        return FirebaseApp.initializeApp(options);
    }

    @ConditionalOnProperty(prefix = "firebase", name = "enabled", havingValue = "true")
    @Bean
    public FirebaseMessaging firebaseMessaging(FirebaseApp firebaseApp) {
        return FirebaseMessaging.getInstance(firebaseApp);
    }

    @Bean
    public FireBaseService fireBaseService(FirebaseMessaging firebaseMessaging) {
        return new FireBaseService(firebaseMessaging);
    }
}
