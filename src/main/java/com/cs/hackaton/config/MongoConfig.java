package com.cs.hackaton.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@EnableReactiveMongoRepositories(basePackages = "com.cs.hackaton.repositories")
@Configuration
public class MongoConfig {
}
