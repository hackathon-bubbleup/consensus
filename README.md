Build, tag, and push Docker image
Now that your repository exists, you can push a Docker image by following these steps:
Successfully created repository
521776600419.dkr.ecr.us-east-1.amazonaws.com/consensus-be
Ensure you have installed the latest version of the AWS CLI and Docker. For more information, see the ECR documentation.
1) Retrieve the login command to use to authenticate your Docker client to your registry.
For macOS or Linux systems, use the AWS CLI:
$(aws ecr get-login --no-include-email --region us-east-1)

For Windows systems, use AWS Tools for PowerShell:
Invoke-Expression -Command (Get-ECRLoginCommand -Region us-east-1).Command

Note: If you receive an "Unknown options: --no-include-email" error when using the AWS CLI, ensure that you have the latest version installed. Learn more
2) If you are using the AWS CLI, run the login command from the output of step 1.
3) Build your Docker image using the following command. For information on building a Docker file from scratch see the instructions here. You can skip this step if your image is already built:
docker build -t consensus-be .

4) After the build completes, tag your image so you can push the image to this repository:
docker tag consensus-be:latest 521776600419.dkr.ecr.us-east-1.amazonaws.com/consensus-be:latest

5) Run the following command to push this image to your newly created AWS repository:
docker push 521776600419.dkr.ecr.us-east-1.amazonaws.com/consensus-be:latest

jenkins
http://54.173.179.211:8080/

gitlab api token - yETSj7tBkVeLY9WTozNm

jenkins api token - 1143608648db369ff6df20e25c2d3a21df